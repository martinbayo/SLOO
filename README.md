Objetivos de la materia.
--------------------

*Esta materia debe ser la primera aproximación para pasar de ser un usuario de clicks a un usuario crítico lo que implica ser capaz de operar/manipular/hacer cosas con el software sobre la base de decisiones reflexionadas

*Ser capaz de probar e instalar GNU/Linux 

*Tener un panorama de las características de algunas distribuciones de GNU/Linux.

*Poder instalar y configurar GNU/Linux en sus aspectos básicos de funcionamiento para una PC de escritorio en un ambiente de oficina.

*Desarrollar la capacidad de crear máquinas virtuales (Vms).

*Probar distintas distribuciones de GNU/Linux mediante máquinas uso Vms.

*Administrar paquetes con herramientas gráficas.

*Instalar y configurar un entorno de escritorio completo. Esto implica conocer e entender diferencias themes, plugins, addons , etc


Temario básico:
--------------------

Utilización de un live cd.

Distribuciones de GNU/Linux.

Entornos gráficos de escritorio.

Modificación y personalización, plugins.

Instalación y configuración de escritorio 

Gestión de paquetes en forma gráfica


Temario expandido
--------------------

Utilización de un live cd.
====================

*Configuración de BIOS

*Selección de orden de booteo

*Busqueda en Internet, paquetes de oficina, configuraciones gráficas


Distribuciones de GNU/Linux.
====================

*Categorización de Distribuciones de GNU/Linux.

  -Por Libertades de Distribuciones

  -Por Características técnicas

  -Por tipos de paquetes (rpm, deb) algo así por sus orígenes

  -Por sus tipos de usuarios


Entornos gráficos de escritorio.
====================

*Distintos entornos 

*Concepto de servidor X ( y si se quiere comparando con Window$)

*Gestores de ventanas  (Kde, Gnome, Mate, Otras)


Modificación y personalización, plugins.
====================

*Plugins

*Add Ons

 
Instalación y configuración de escritorio 
====================

*VirtualBox

*Particiones

*Sectores de booteo

*configuraciones de escritorio y “temas”

*Personalización de cada usuario o por sistema

Gestión de paquetes en forma gráfica
====================

*Tipos de paquetes

*Repositorios

*Gestores de paquetes