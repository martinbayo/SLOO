================================================
Tecnicatura Universitaria en Software Libre
================================================
------------------------------------------------
Software Libre: Ofimática en las organizaciones
------------------------------------------------

:Docente: Javier Burgos

.. header:: 
  Software Libre: Ofimática en las organizaciones

.. contents:: Contenido

.. raw:: pdf

   PageBreak oneColumn

1. Presentación 
================
Este es el primer contacto que tiene con GNU/Linux en la Tecnicatura Universitaria en Software Libre.
Trataremos de lograr que ud. pueda, sin mayores conocimientos técnicos, probar GNU/Linux sin instalarlo utilizando un LiveCD y luego sea capaz de instalarlo en una PC.

Esta materia tiene un enfoque que va desde lo práctico a los contenidos teóricos mínimos, de modo que le brinden una primer experiencia y la posibilidad de que ud. mismo tenga un sistema funcionando para que pueda explorar y avanzar todo lo que desee. 

Para tener una guía única sobre el proceso de instalación utilizaremos GNU/Linux Ubuntu, pero veremos otras distribuciones orientadas a usuarios finales. 

Una vez instalado GNU/Linux, avanzaremos en la instalación de algunas herramientas comunes de ofimática (libreoffice, navegadores web, etc) y de multimedia. De esta manera, finalmente será capaz de tener un GNU/Linux Desktop totalmente funcional.

Para que el proceso de instalación de distintas distribuciones sea más práctico, utilizaremos VirtualBox para crear máquinas virtuales sobre las cuales podrá probar y comparar distintas distribuciones de GNU/Linux.

Al finalizar esta materia ud. habrá dado los primeros pasos en el mundo GNU/Linux, eligiendo una distribución dentro la riqueza y variedad del mundo del Software Libre que será capaz de instalar y por supuesto, compartir estos logros con toda la comunidad!.

Recuerde que el presente material es una guía y que también cuenta con el foro de la plataforma virtual donde podrá consultar sus dudas y exponer sus experiencias.

2. Programa
============
3. Objetivos
=============

* Ser capaz de probar e instalar GNU/Linux Ubuntu y/o Xubuntu
* Tener un panorama de las caracaterísticas de algunas distribuciones de GNU/Linux.
* Poder instalar y configurar GNU/Linux Ubuntu en sus aspectos básicos de funcionamiento para una PC de escritorio en un ambiente de oficina.
* Utilizar VirtualBox como herramienta de creación de máquinas virtuales (VMs). Probar instalaciones de distintas distribuciones de Linux en el ambiente de pruebas de las VMs.
* Administrar paquetes con herramientas gráficas.
 
4. Contenidos
==============
4.1 Módulo 1: Comenzando con GNU/Linux 
---------------------------------------
Como planteamos en la presentación de la materia, daremos un enfoque práctico, por tanto, empezaremos a introducirnos con Linux probándolo.

Antes revisaremos algunas ideas que nos ayudarán a organizarnos; a media que avancemos, profundizaremos en las mismas y veremos otros conceptos.

GNU/Linux es un Sistema Operativo y sobre el mismo un conjunto de Software de aplicación muy variado. Es un tipo de sistema operativo basado en Unix pero como indica su acrónimo GNU: "No es Unix".

Las distintas formas de organizar esas aplicaciones, en base a ciertos principios, es lo que dá origen a las denominadas Distribuciones de GNU/LInuxcomo Debian, RedHat, Centos, Ubuntu, y muchísimas otras más. Algunas distribuciones se basan sobre otras, por ejemplo Ubuntu sobre Debian, y otras son derivadas, por ejemplo, Centos de RedHat.

Ubuntu es una distribución de GNU/Linux que actualmente puede ser usada tanto en PC (desktops y servidores) como en tablets, celulares y TVs. Es muy popular y con un enfoque puesto en la facilidad de instalación y uso. Si bien existen distribuciones que se ajustan mejores a las distintas necesidades, Ubuntu es una buena forma de comenzar en el maravilloso mundo GNU/Linux.

4.1.1 Ubuntu en Live CD
........................
Un LiveCD de Ubuntu nos permite ejecutar el Sistema Operativo y todas sus aplicaciones incluídas sin necesidad de instalar nada en nuestro disco rígido. Es una buena manera de comenzar a ver y probar GNU/Linux.

Los requisitos de hardware para ejecutar Ubuntu son los siguientes:

* Procesador x86 a 700 MHz.
* Memoria RAM de 512 Mb.
* Disco Duro de 5 Gb.
* Tarjeta gráfica y monitor capaz de soportar una resolución de 1024×768.
* Lector de DVD o puerto USB.
* Conexión a internet.

**Nota:**

Si su PC no cumple con los requisitos mínimos, no se preocupe, puede pasar a la sección 4.1.3 en donde verá Xubuntu, una distribución basada en Ubuntu que tiene menores requerimientos de hardware.

Primeramente obtendremos un archivo con formato ISO que contiene Ubuntu, luego lo grabaremos en un CD con el programa que ud. prefiera.
Puede descargar dicho archivo de la web de ubuntu.com en esta dirección:

http://www.ubuntu.com/download/desktop

Utilizaremos la versión Ubuntu 14.04.2 LTS para la aquitectura de 64bits, muy probablemente sea la correcta para su PC.

Una vez grabada la imagen ISO en CD, debe iniciar la PC desde la unidad de CD/DVD ROM. Para esto muchas PCs, antes de que inicie el Sistema Operativo, muetran un mensaje indicando con que tecla se lanza el selector de booteo, puede ser F8, o F11, dependiendo de la placa madre.

.. figure:: images/bios01.png
    :align: center

Si la arquitectura de su PC es de 32bits y está iniciando con Ubuntu para 64bits, recibirá un mensaje de error como el siguiente:

.. figure:: images/error_kernel_32.png
    :width: 1440px
    :align: center
    :height: 150px 

Deberá bajar la ISO correspondiente desde http://www.ubuntu.com/download/desktop (seleccione de la lista desplegable la opción para 32bits)

Al iniciar Ubuntu, se muestra la siguiente pantalla donde elegiremos el idioma español:

.. figure:: images/ubuntu1.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego seleccionamos la opción que dice "Probar Ubuntu" y el sistema nos dejará en el escritorio:

.. figure:: images/ubuntu2.png
    :width: 1440px
    :align: center
    :height: 1080px

Puede comnezar a investigar y probar por su propia cuenta, recuerde que nada se instala en el disco rígido.

4.1.2 Ubuntu en Pen Drive
.........................
Desde el LiveCD ejecutándose podemos pasar todo el sistema a un Pen Drive para luego ejecutarlo desde el mismo, que es más rápido y confiable.

Para esto buscamos el creador de imagenes de arranque:

4.1.3 Xubuntu Live CD
......................
Xubuntu es una distribución Linux basada en Ubuntu que utiliza un ligero entorno de escritorio llamado Xfce, mucho mas rápido y sencillo que el Unity de Ubuntu. Está diseñado para poder ser usado en PC no tan modernas. La versión actual es la 14.04, la podemos descargar de la siguiente url:

http://xubuntu.org/getxubuntu/

Una vez descargada la imagen ISO, la puede grabar en un CD e iniciar la instalación booteando la PC desde la lectora de CD. Este proceso es igual que el del punto 4.1.1.

Veamos la primer pantalla de Xubuntu:

Como podemos ver nos ofrece probar Xubuntu (inicia como Live CD, sin instalación) o directamente instalarlo.

.. figure:: images/xubuntu1.png
    :width: 1440px
    :align: center
    :height: 1080px

Seleccionaremos el idioma y luego "Probar Ubuntu", el sistema iniciará y nos mostrará el escritorio. En la siguiente pantalla podemos ver desplegado el menú que nos muestra las aplicaciones con las que contamos:

.. figure:: images/xubuntu2.png
    :width: 1440px
    :align: center
    :height: 1080px

4.2 Módulo 2: VirtualBox
-------------------------
Antes de avanzar con los siguientes módulos, veremos una herramienta que le servirá para hacer pruebas de instalaciones de cualquier distribución de GNU/Linux: VirtualBox.

VirtualBox nos permite crear máquinas virtuales a las cuales se les asignan recursos de sistemas virtuales: memoria RAM, disco rígido, placas de red, y otros. No se pretende profundizar en conocimientos de virtualización (los verá en materias siguientes) sino contar con un ambiente de pruebas que le facilite la instalación de distintos Sistemas Operativos GNU/Linux.

Para instalar VirtualBox:

* En windows: vaya a la url https://www.virtualbox.org/wiki/Downloads. La versión actual disponible la puede descargar donde dice "VirtualBox 4.3.26 for Windows hosts  x86/amd64". Luego ejecute el instalador (archivo .exe) y continúe con el proceso de instalación.
* Si tiene Linux instalado, puede instalar VirtualBox directamente a través del administrador de paquetes que tenga disponible en su distribución.

4.2.1 Creación de máquinas virtuales (VMs)
...........................................
Luego de iniciar VirtualBox, elegimos la opción para crear una nueva VM y especificamos el tipo de Sistema Operativo, en este caso crearemos una VM para Ubuntu de 64 bits:

.. figure:: images/vbox1.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego especificamos la memoria RAM que le asignaremos a la VM, esta cantidad está limitada por la memoria dispobible de su sistema:

.. figure:: images/vbox2.png
    :width: 1440px
    :align: center
    :height: 1080px

Definimos ahora el disco virtual para la PC:

.. figure:: images/vbox3.png
    :width: 1440px
    :align: center
    :height: 1080px

Y el tipo de disco, en este caso VDI, que es un formato propio de virtualbox que permite mayor velocidad, como contrapartida no lo podemos migrar sin convertir a otra herramienta de virtualización:

.. figure:: images/vbox4.png
    :width: 1440px
    :align: center
    :height: 1080px

Si no cuenta con mucho espacio en disco disponible, puede definir el tamaño como dinámico, lo cual ahorra espacio pero por contrapartida es más lento. En este caso, no necesitamos mucho espacio para probar esta VM, con 12GB nos alcanza para probar la distribución y algunas aplicaciones extras, por tanto lo definiremos como "tamaño fijo" para que sea más rápida la ejecución de la VM:

.. figure:: images/vbox5.png
    :width: 1440px
    :align: center
    :height: 1080px

.. figure:: images/vbox6.png
    :width: 1440px
    :align: center
    :height: 1080px

Puede demorar unos minutos la creación del espacio reservado para sistema:

.. figure:: images/vbox7.png
    :width: 1200px 
    :align: center
    :height: 280px

Luego vamos a asignar una imagen iso de Ubuntu; con esto simularemos que estamos poniendo el CD de instalación de Ubuntu en la lectora de CD/DVD ROM. 
Para esto seleccionamos la VM creada, luego configuración, y luego "Almacenamiento", dentro de esta ventana configuramos como sigue:

.. figure:: images/vbox8.png
    :width: 1440px
    :align: center
    :height: 1080px

La útima configuración es para que inicie desde la unidad de CD/DVD ROM, en realidad iniciará desde la imagen ISO que previamente asignamos:

.. figure:: images/vbox9.png
    :width: 1440px
    :align: center
    :height: 1080px

Finalmente iniciamos la VM y debería iniciar la instalación de Ubuntu en nuestra VM! Puede crear otras VMs y asignarles imágenes ISO de la distribución que desee para probar de manera sencilla el proceso de instalación.

En el siguiente módulo explicaremos como avanzar con el proceso de instalación iniciado. 

4.3 Módulo 3: Instalación de GNU/Linux: Ubuntu y Xubuntu en VirtualBox 
-----------------------------------------------------------------------
4.2.1 Instalación de Ubuntu en una Máquina virtual (VM) de VirtualBox
........................................................................

Comenzaremos instalando Ubuntu en la VM creada en la sección 4.2.1 

Luego de asociar la imagen iso de ubuntu e iniciar la VM, elegiremos el idioma e "Instalar Ubuntu" como podemos ver en la pantalla siguiente:

 .. figure:: images/ubuntu1.png
    :width: 1440px
    :align: center
    :height: 1080px

Nos aparecerá la pantalla siguiente, en donde podemos ver los requisitos mínimos para poder llevar adelante la instalción:

.. figure:: images/ubuntu3.png
    :width: 1440px
    :align: center
    :height: 1080px

Si la PC no tiene ningún sistema operativo instalado, nos aparecerá la siguiente pantalla ofreciéndonos borrar el disco e instalar Ubuntu

.. figure:: images/ubuntu4.png
    :width: 1440px
    :align: center
    :height: 1080px

Probablemente ud. ya cuente con un Sistema Operativo, en cuyo caso nos ofrece instalar Ubuntu junto al Sistema Operativo Existente:

Recuerde que antes de hacer esto es **recomendable** que ud. cuente con un backup.

.. figure:: images/ubuntu5.png
    :width: 1440px
    :align: center
    :height: 1080px

A continuación nos muestra los espacios que podrán ocupar cada uno de los sistemas operativos y podremos ajustar dichos tamaños dentro de los límites permitidos por el uso de los datos:

.. figure:: images/ubuntu6.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego se mostrará una pantalla donde nos informa sobre los cambios que realizará y seleccionaremos "Continuar":

.. figure:: images/ubuntu7.png
    :width: 1440px
    :align: center
    :height: 1080px

Ahora elegimos la localización (debería proponer la correcta: Buenos Aires):

.. figure:: images/ubuntu8.png
    :width: 1440px
    :align: center
    :height: 1080px

Y luego la distribución de nuestro teclado, si no está seguro, pruebe con Español/Latinoamericano:

.. figure:: images/ubuntu9.png
    :width: 1440px
    :align: center
    :height: 1080px

A continuación elegiremos el nombre, usuario y una contraseña. Es recomendable una cotraseña que al menos contenga mayúsculas, minúsculas y números, por ejemplo: Usuari0

.. figure:: images/ubuntu10.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego continuará la instalación de Ubuntu:

.. figure:: images/ubuntu11.png
    :width: 1440px
    :align: center
    :height: 1080px

Finalmente tendremos un aviso de instalación exitosa pediéndonos reiniciar la PC:

 .. figure:: images/ubuntu12.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego de hacer click en "Reiniciar", nos mostrará un mensaje:

 .. figure:: images/ubuntu13.png
    :width: 1440px
    :align: center
    :height: 1080px

Esto indica que debemos quitar el CD o Pen drive para que no vuelva a iniciar desde el mismo.

Finalmente, al reiniciar la PC, nos debería mostrar un menú en donde elegiremos iniciar con nuestro nuevo GNU/Ubuntu!

 .. figure:: images/ubuntu14.png
    :width: 1440px
    :align: center
    :height: 1080px

Una vez que inicia Ubuntu, nos pedirá la contraseña del usuario que hemos creado durante la instalación:

 .. figure:: images/ubuntu15.png
    :width: 1440px
    :align: center
    :height: 1080px

Una vez puesta la contraseña, se inicia la sesión y ya está en el escritorio de Ubuntu para comenzar a utilizarlo. Felicitaciones.

4.2.2 Instalación de Xubuntu
..............................
Si desea instalar Xubuntu, inicie la PC con el CD creado en la sección 4.1.3 y luego de seleccionar el idioma, en vez de seleccionar "Probar Xubuntu", seleccionaremos "Instalar Xubuntu" que comenzará el proceso de instalación como sigue:

.. figure:: images/xubuntu3.png
    :width: 1440px
    :align: center
    :height: 1080px

Nos aparecerá la pantalla siguiente, en donde podemos ver los requisitos mínimos para poder llevar adelante la instalción:

 .. figure:: images/ubuntu5.png
    :width: 1440px
    :align: center
    :height: 1080px

Si la PC no tiene ningún sistema operativo instalado, nos aparecerá la siguiente pantalla ofreciéndonos borrar el disco e instalar Xubuntu:

 .. figure:: images/xubuntu6.png
    :width: 1440px
    :align: center
    :height: 1080px

Probablemente ud. ya cuente con un Sistema Operativo, en cuyo caso nos ofrece instalar Xubuntu junto al Sistema Operativo Existente:

Recuerde que antes de hacer esto es **recomendable** que ud. cuente con un backup.

 .. figure:: images/ubuntu5.png
    :width: 1440px
    :align: center
    :height: 1080px

A continuación nos muestra los espacios que podrán ocupar cada uno de los sistemas operativos y podremos ajustar dichos tamaños dentro de los límites permitidos por el uso de los datos:

 .. figure:: images/ubuntu6.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego se mostrará una pantalla donde nos informa sobre los cambios que realizará y seleccionaremos "Continuar":

 .. figure:: images/ubuntu7.png
    :width: 1440px
    :align: center
    :height: 1080px

Ahora elegimos la localización (debería proponer la correcta: Buenos Aires):

 .. figure:: images/ubuntu8.png
    :width: 1440px
    :align: center
    :height: 1080px

Y luego la distribución de nuestro teclado, si no está seguro, pruebe con Español/Latinoamericano:

 .. figure:: images/ubuntu9.png
    :width: 1440px
    :align: center
    :height: 1080px

A continuación elegiremos el nombre, usuario y una contraseña. Es recomendable una cotraseña que al menos contenga mayúsculas, minúsculas y números, por ejemplo: Usuari0

 .. figure:: images/xubuntu7.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego continuará la instalación de Xubuntu:

 .. figure:: images/xubuntu8.png
    :width: 1440px
    :align: center
    :height: 1080px

En la siguiente pantalla podemos las principales aplicaciones que trae nuestro Xubuntu:

 .. figure:: images/xubuntu8b.png
    :width: 1440px
    :align: center
    :height: 1080px

Finalmente tendremos un aviso de instalación exitosa pediéndonos reiniciar la PC:

 .. figure:: images/xubuntu9.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego de hacer click en "Reiniciar", nos mostrará un mensaje:

 .. figure:: images/xubuntu10.png
    :width: 1440px
    :align: center
    :height: 1080px

Esto indica que debemos quitar el CD o Pen drive para que no vuelva a iniciar desde el mismo.

Finalmente, al reiniciar la PC, nos debería mostrar un menú en donde elegiremos iniciar con nuestro nuevo GNU/Xubuntu!

 .. figure:: images/ubuntu14.png
    :width: 1440px
    :align: center
    :height: 1080px

Si no existen otros sistemas operativos instalados, Xubuntu iniciará directamente.

Una vez que inicia Xubuntu, nos pedirá la contraseña del usuario que hemos creado durante la instalación:

 .. figure:: images/xubuntu11.png
    :width: 1440px
    :align: center
    :height: 1080px

Una vez puesta la contraseña, se inicia la sesión y ya está en el escritorio de Xubuntu para comenzar a utilizarlo. Felicitaciones.


4.2.3 Instalación de Ubuntu en el disco rígido
..............................................

Si ud prefiere instalar Ubuntu directamente en la PC en vez de una VM de VirtualBox y ya tiene otro sistema operativo funcionando (como Windows XP, 7, 8, etc) debe saber que, si bien Ubuntu intenta preservar el sistema previamente instalado junto con los datos, es MUY RECOMENDABLE que tenga copia de 
seguridad, puesto que no está garantizado que no se pierdan datos. 

En realidad no es que sea un riesgo específico de Ubuntu sino que es una regla general de la informática: al trabajar con particiones y filesystems debe primeramente hacer copia de seguridad por el riesgo que implica este tipo de operación.

Ahora que está advertido y que ud. está decido a realizar una instalación de GNU/Linux en un ambiente real, pasaremos a mostrar el procedimiento
 
inicie nuevamente con el CD de Ubuntu que creó en la sección 4.1.1.

Si prefiere iniciar con el Pen Drive creado en la sección 4.1.2, su PC debe permitir el inicio desde este dispositivo.

Luego de iniciar con el medio que haya seleccionado, elegiremos el idioma e "Instalar Ubuntu" como podemos ver en la pantalla siguiente:

 .. figure:: images/ubuntu1.png
    :width: 1440px
    :align: center
    :height: 1080px

Nos aparecerá la pantalla siguiente, en donde podemos ver los requisitos mínimos para poder llevar adelante la instalción:

 .. figure:: images/ubuntu3.png
    :width: 1440px
    :align: center
    :height: 1080px

Si la PC no tiene ningún sistema operativo instalado, nos aparecerá la siguiente pantalla ofreciéndonos borrar el disco e instalar Ubuntu

 .. figure:: images/ubuntu4.png
    :width: 1440px
    :align: center
    :height: 1080px

Probablemente ud. ya cuente con un Sistema Operativo, en cuyo caso nos ofrece instalar Ubuntu junto al Sistema Operativo Existente:

Recuerde que antes de hacer esto es **recomendable** que ud. cuente con un backup.

 .. figure:: images/ubuntu5.png
    :width: 1440px
    :align: center
    :height: 1080px

A continuación nos muestra los espacios que podrán ocupar cada uno de los sistemas operativos y podremos ajustar dichos tamaños dentro de los límites permitidos por el uso de los datos:

 .. figure:: images/ubuntu6.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego se mostrará una pantalla donde nos informa sobre los cambios que realizará y seleccionaremos "Continuar":

 .. figure:: images/ubuntu7.png
    :width: 1440px
    :align: center
    :height: 1080px

Ahora elegimos la localización (debería proponer la correcta: Buenos Aires):

 .. figure:: images/ubuntu8.png
    :width: 1440px
    :align: center
    :height: 1080px

Y luego la distribución de nuestro teclado, si no está seguro, pruebe con Español/Latinoamericano:

 .. figure:: images/ubuntu9.png
    :width: 1440px
    :align: center
    :height: 1080px

A continuación elegiremos el nombre, usuario y una contraseña. Es recomendable una cotraseña que al menos contenga mayúsculas, minúsculas y números, por ejemplo: Usuari0

 .. figure:: images/ubuntu10.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego continuará la instalación de Ubuntu:

 .. figure:: images/ubuntu11.png
    :width: 1440px
    :align: center
    :height: 1080px

Finalmente tendremos un aviso de instalación exitosa pediéndonos reiniciar la PC:

 .. figure:: images/ubuntu12.png
    :width: 1440px
    :align: center
    :height: 1080px

Luego de hacer click en "Reiniciar", nos mostrará un mensaje:

 .. figure:: images/ubuntu13.png
    :width: 1440px
    :align: center
    :height: 1080px

Esto indica que debemos quitar el CD o Pen drive para que no vuelva a iniciar desde el mismo.

Finalmente, al reiniciar la PC, nos debería mostrar un menú en donde elegiremos iniciar con nuestro nuevo GNU/Ubuntu!

 .. figure:: images/ubuntu14.png
    :width: 1440px
    :align: center
    :height: 1080px

Una vez que inicia Ubuntu, nos pedirá la contraseña del usuario que hemos creado durante la instalación:

 .. figure:: images/ubuntu15.png
    :width: 1440px
    :align: center
    :height: 1080px

Una vez puesta la contraseña, se inicia la sesión y ya está en el escritorio de Ubuntu para comenzar a utilizarlo. Felicitaciones.


4.2.3 Actualización del Sistema
................................
Tanto en Ubuntu como en Xubuntu, nos puede aparecer una pantalla como la que sigue, ofreciéndonos actualizar el sistema:

 .. figure:: images/xubuntu12.png
    :width: 1440px
    :align: center
    :height: 1080px

Siempre es recomendable mantener nuestro sistema actualizado por cuestiones de seguridad y para tener los últimos drivers y mejoras disponibles. Al hacer click en "Instalar ahora" nos pedirá nuestra contraseña. Es oportuno mencionar que para operaciones críticas, el sistemas siempre nos pedirá la contraseña.

Si no nos aparece el mensaje, podemos nosotros buscar la actualización:

**Actualizar Xubuntu**

 .. figure:: images/xubuntu13.png
    :width: 1440px
    :align: center
    :height: 1080px

**Actualizar Ubuntu**

4.2.4 Instalación de controladores adicionales.
..................................................
4.3 Módulo 3: Otras distribuciones de GNU/Linux
-------------------------------------------------
4.3.1 Debian
.............
4.3.2 CentOS
.............
4.3. Clasificación de las ditribuciones
.......................................
4.4 Módulo 4: Ubicación y personalización del entorno de trabajo en Ubuntu
---------------------------------------------------------------------------
4.4.1 Áreas de trabajo 
......................
4.4.2 El escritorio
....................
4.4.3 Ventanas
...............
4.4.4 Paneles
..............
4.5 Módulo 5: Administración básica del sistema 
-----------------------------------------------
Red
....
Fecha, hora
..................
teclado, idioma
..................

4.6 Módulo 6: Gestión de software (paquetes) en forma gráfica
---------------------------------------------------------------
4.6.1 Herramientas
...................

**Centro de software**

Instalar utilidades
....................
instalar plugins para firefox
..............................
**Instalar Flash**

**Instalar Java**

Instalar códecs multimedia
...........................

4.7 Módulo 7: Explorador de archivos
-------------------------------------------

4.8 Módulo 8: Ofimática disponible en Ubuntu 
----------------------------------------------
4.8.1 Libreoffice
..................
4.8.2 Firefox
..............

Módulo 9: Multimedia 
---------------------
* VLC

Módulo 10: Utilidades
----------------------
* Notas
* Calendario
* Compresor de archivos

5. Trabajo Práctico
===================

1) Elegir una distribución de GNU/Linux distinta de las mostradas en este material, luego:

* Detalle las características:
  * Entorno gráfico
  * Concepción de la misma: usuario final, servidores, multimedia, servicios de redes (firewall, VPN), etc
* Realizar su instalación en una VM (Máquina Virtual) de VirtualBox. Se deben capturar pantallas del proceso de instalación y justificar las decisiones tomadas a lo largo del mismo.

2) Escoja 2 distribuciones de Linux en sus versiones actuales y realice un informe detallando de cada una:

* Si viene con entorno gráfico y cual (KDE, Gnome, xfce, unity, ldm, fluxbox, etc)
* Aplicaciones ofimáticas
* Formato de paquete que utiliza (rpm, deb)
* Versión del kernel

6. Trabajo/Exámen Final (presencial)
=====================================
La aprobación de este trabajo es necesaria para aprobar la materia.

Dependiendo de la cantidad de alumnos, se formarán grupos que deberán hacer un demo install de la distribución que elijan en una PC física y explicar al resto de los alumnos presentes.

7. Bibliografía
================
* http://www.ubuntu.com/
* http://xubuntu.org/
* http://www.esdebian.org/
* http://www.centos.org/docs/
* http://es.wikipedia.org/wiki/CentOS
* http://www.linux-es.org/
