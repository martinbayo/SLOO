COMMON=common
R2PFLAGS=-s kerning,$(COMMON)/doc_fich.style --smart-quotes=1 --fit-background-mode=scale

.PHONY: clean

%.pdf: %.rst FORCE
	rst2pdf $< $(R2PFLAGS)

clean:
	rm -Ivr */*pdf

FORCE:
